﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class RandomLevelSeed
{
    public bool Unique;
    public bool Final;
    public int UniqueId;
    public int Width;
    public int Height;
    public int Mountains;
    public int TurnsUntilSD = 10;
    public Vector3Int StartingPoint;
    public Dictionary<GroundObjectsType, int> Objects;
    public Dictionary<EnemyType, int> Enemies;
    public int[] SDSpawnSpread = new int[] { 0, 0, 1, 1, 2 };
    public int EnemiesDistanceFromBorder;
    public int Survivors;

    public Vector3Int BaseLocation { get; internal set; }

    internal RandomLevelSeed Modify(Action<RandomLevelSeed> cb)
    {
        cb(this);
        return this;
    }
}

public static class LevelProgression
{
    public static RandomLevelSeed[] Levels =
    {
         // 0 - tutorial
         new RandomLevelSeed { Width = 10, Height = 8, Mountains = 6,
            Enemies = new Dictionary<EnemyType, int> {
                {EnemyType.Basic, 3}
            },
            EnemiesDistanceFromBorder = 1,
            TurnsUntilSD = 18,
            Objects = new Dictionary<GroundObjectsType, int> {
                {GroundObjectsType.Residence, 4}
            }
        },
        
         // 1 - Basic survival
        LightLevel().Modify(d => d.Survivors = 1),
        
        // 2 
        LightLevel(),
        
        // 3 Gauntlet
        LightLevel().Modify(d => {
            d.Enemies[EnemyType.Basic] = 0;
            d.TurnsUntilSD = 1;
            d.Objects[GroundObjectsType.Mall] = 1;
            d.SDSpawnSpread = new int[] { 1, 1, 1, 1, 2 };
        }),


        // 4 Repair 
        MediumLevel().Modify(d => {
            d.Objects[GroundObjectsType.Factory] = 2;
            d.Enemies[EnemyType.Watcher] = 1;
            d.Enemies[EnemyType.Basic] = 3;
        }),
        // 5 
        MediumLevel(),

        // 6 Runners
        MediumLevel().Modify(d => {
            d.Enemies[EnemyType.Basic] = 0;
            d.Enemies[EnemyType.Runner] = 4;
            d.Objects[GroundObjectsType.Mall] = 2;
            d.TurnsUntilSD = 22;
        }),

        // 7 MidBoss
        MediumLevel().Modify(d => {
            d.Enemies[EnemyType.Basic] = 3;
            d.Enemies[EnemyType.Watcher] = 2;
            d.Enemies[EnemyType.Fatty] = 1;
            d.TurnsUntilSD = 22;
        }),

        // 8 Breather
        MediumLevel().Modify(d => {
            d.Enemies[EnemyType.Basic] = 1;
            d.Objects[GroundObjectsType.Mall] = 2;
            d.Objects[GroundObjectsType.Factory] = 2;
        }),

        // 9 
        HardLevel(),
        //10
        HardLevel().Modify(d => {
            d.Final = true;
            d.TurnsUntilSD = 1;
            d.StartingPoint = new Vector3Int(9,3,0);
        })

    };

    public static RandomLevelSeed LightLevel()
    {
        var Width = UnityEngine.Random.Range(10, 13);
        return new RandomLevelSeed
        {
            Width = Width,
            Height = Width - 2,
            Mountains = UnityEngine.Random.Range(3, 12),
            EnemiesDistanceFromBorder = 1,
            TurnsUntilSD = 12,
            SDSpawnSpread = new int[] { 0, 0, 0, 1, 1 },
            Enemies = new Dictionary<EnemyType, int> {
                { EnemyType.Basic, UnityEngine.Random.Range(3, 5) }
            },
            Survivors = UnityEngine.Random.Range(0, 1),
            Objects = new Dictionary<GroundObjectsType, int> {
                {GroundObjectsType.Residence, UnityEngine.Random.Range(3, 5)}
            }
        };
    }

    public static RandomLevelSeed MediumLevel()
    {
        var Width = UnityEngine.Random.Range(14, 18);
        return new RandomLevelSeed
        {
            Width = Width,
            Height = Width - UnityEngine.Random.Range(2, 3),
            Mountains = UnityEngine.Random.Range(6, 16),
            EnemiesDistanceFromBorder = 2,
            SDSpawnSpread = new int[] { 0, 0, 1, 1, 2 },
            TurnsUntilSD = 20,
            Enemies = new Dictionary<EnemyType, int> {
                { EnemyType.Basic, UnityEngine.Random.Range(3, 4) },
                { EnemyType.Watcher, UnityEngine.Random.Range(0, 1) },
                { EnemyType.Runner, UnityEngine.Random.Range(1, 2) }
            },
            Survivors = UnityEngine.Random.Range(1, 2),
            Objects = new Dictionary<GroundObjectsType, int> {
                {GroundObjectsType.Residence, UnityEngine.Random.Range(6, 10)},
                {GroundObjectsType.Mall, UnityEngine.Random.Range(0, 1)},
                {GroundObjectsType.Factory, UnityEngine.Random.Range(0, 1)}
            }
        };
    }

    public static RandomLevelSeed HardLevel()
    {
        var Width = UnityEngine.Random.Range(16, 20);
        return new RandomLevelSeed
        {
            Width = Width,
            Height = Width - UnityEngine.Random.Range(2, 4),
            Mountains = UnityEngine.Random.Range(3, 12),
            EnemiesDistanceFromBorder = 3,
            TurnsUntilSD = 18,
            SDSpawnSpread = new int[] { 1, 1, 1, 2, 2 },
            Enemies = new Dictionary<EnemyType, int> {
                { EnemyType.Basic, UnityEngine.Random.Range(3, 5) },
                { EnemyType.Watcher, UnityEngine.Random.Range(0, 2) },
                { EnemyType.Runner, UnityEngine.Random.Range(1, 2) },
                { EnemyType.Fatty, UnityEngine.Random.Range(1, 2) }
            },
            Survivors = UnityEngine.Random.Range(2, 3),
            Objects = new Dictionary<GroundObjectsType, int> {
                {GroundObjectsType.Residence, UnityEngine.Random.Range(8, 12)},
                {GroundObjectsType.Mall, UnityEngine.Random.Range(0, 2)},
                {GroundObjectsType.Factory, UnityEngine.Random.Range(0, 2)}
            }
        };
    }
}