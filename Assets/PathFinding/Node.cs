﻿namespace PathFind
{
    /**
    * A node in the grid map
    */
    public class Node
    {
        // node starting params
        public bool walkable;
        public int gridX;
        public int gridY;
        public float _price;
        public float penalty
        {
            get { return _price + (enemy ? 100 : 0); }
        }
        // calculated values while finding path
        public int gCost;
        public int hCost;
        public Node parent;

        public int areaCost;
        internal bool enemy;

        // create the node
        // _price - how much does it cost to pass this tile. less is better, but 0.0f is for non-walkable.
        // _gridX, _gridY - tile location in grid.
        public Node(float price, int _gridX, int _gridY)
        {
            walkable = price != 0.0f;
            _price = price;
            gridX = _gridX;
            gridY = _gridY;
        }

        public int fCost
        {
            get
            {
                return gCost + hCost;
            }
        }
    }
}