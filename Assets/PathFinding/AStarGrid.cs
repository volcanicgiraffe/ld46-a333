﻿using UnityEngine;
using System.Collections.Generic;
using System;

namespace PathFind
{
    /**
    * The grid of nodes we use to find path
    */
    public class AStarGrid
    {
        public Node[,] nodes;
        public List<Node> dynamicNodes = new List<Node>();
        int gridSizeX, gridSizeY;

        /**
        * Create a new grid with tile prices.
        * width: grid width.
        * height: grid height.
        * tiles_costs: 2d array of floats, representing the cost of every tile.
        *               0.0f = unwalkable tile.
        *               1.0f = normal tile.
        */
        public AStarGrid(int width, int height, GroundTileModel[,] tiles_costs)
        {
            gridSizeX = width;
            gridSizeY = height;
            nodes = new Node[width, height];

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    var price = tiles_costs[x, y].speedPrice;
                    nodes[x, y] = new Node(price, x, y);
                }
            }
        }

        /**
        * Create a new grid of just walkable / unwalkable.
        * width: grid width.
        * height: grid height.
        * walkable_tiles: the tilemap. true for walkable, false for blocking.
        */
        public AStarGrid(int width, int height, bool[,] walkable_tiles)
        {
            gridSizeX = width;
            gridSizeY = height;
            nodes = new Node[width, height];

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    nodes[x, y] = new Node(walkable_tiles[x, y] ? 1.0f : 0.0f, x, y);
                }
            }
        }

        public List<Node> GetNeighbours(Node node, bool diagonals = false)
        {
            List<Node> neighbours = new List<Node>();

            for (int x = -1; x <= 1; x++)
            {
                for (int y = -1; y <= 1; y++)
                {
                    if (x == 0 && y == 0)
                        continue;

                    if (Mathf.Abs(x) + Mathf.Abs(y) > 1 && !diagonals)
                        continue;

                    int checkX = node.gridX + x;
                    int checkY = node.gridY + y;

                    if (checkX >= 0 && checkX < gridSizeX && checkY >= 0 && checkY < gridSizeY)
                    {
                        neighbours.Add(nodes[checkX, checkY]);
                    }
                }
            }

            return neighbours;
        }

        internal void ClearEnemies()
        {
            foreach (var n in dynamicNodes)
                n.enemy = false;
            dynamicNodes.Clear();
        }

        internal void SetEnemy(Vector3Int vec)
        {
            if (vec.x < 0 || vec.x >= gridSizeX || vec.y < 0 || vec.y >= gridSizeY) return;
            nodes[vec.x, vec.y].enemy = true;
            dynamicNodes.Add(nodes[vec.x, vec.y]);
        }

        public void Clear()
        {
            for (int x = 0; x < gridSizeX; x++)
            {
                for (int y = 0; y < gridSizeY; y++)
                {
                    nodes[x, y].gCost = 0;
                    nodes[x, y].hCost = 0;
                    nodes[x, y].parent = null;
                    nodes[x, y].areaCost = 0;
                }
            }
        }
    }
}