﻿using System;
using UnityEngine;
using UnityEngine.Tilemaps;

public enum GroundType
{
    Ground = 0,
    Mountain = 1
}

public enum GroundObjectsType
{
    Residence = 0,
    Mall = 1,
    Factory = 2,
    Base = 3
}

public enum EnemyType
{
    Basic = 0,
    Watcher = 1,
    Runner = 2,
    Fatty = 3
}

[Serializable]
public class Tileset
{
    // ground
    public GroundTileModel[] GroundTiles = new GroundTileModel[0];
    public GroundTileModel[] MountainTiles = new GroundTileModel[0];
}

[Serializable]
public class GroundTileModel {
    public Tile tile;
    public GroundType type = GroundType.Ground;
    public float speedPrice = 1;
}
