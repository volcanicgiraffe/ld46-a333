﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI : MonoBehaviour
{
    public TMPro.TextMeshProUGUI BotLog;

    public Slider CoreHP;
    public Text CoreHPText;
    Image CoreHPCol;

    public Text BotScrap;
    public Text BotScrapD;

    public Text BotMen;
    public Text BotMenD;

    public Text BotFood;
    public Text BotFoodD;

    public Text BotGunName;
    public Text BotGunStatus;

    public Text BotGun2Name;
    public Text BotGun2Status;

    public Text BotEngineStatus;

    public Text PartyRadio;
    public Text PartyScrap;
    public Text PartyFood;

    public Slider PartyPeople;
    public Text Turn;

    public ScrollRect scroll;

    private LevelBrain lb;

    Queue<string> logs = new Queue<string>();


    public Dictionary<string, float> LogCooldowns = new Dictionary<string, float>();

    // Start is called before the first frame update
    void Start()
    {
        CoreHPCol = CoreHP.GetComponentInChildren<Image>();
        EventManager.Subscribe("*", LogPhrase);
    }

    // Update is called once per frame
    void Update()
    {
        if (lb == null) { lb = LevelBrain.Instance; }
        //LogDebug();
        if (lb.Bot != null)
        {
            CoreHPText.text = $"{lb.Bot.Health:0.#}%";
            CoreHP.value = lb.Bot.Health / 100;
            if (CoreHP.value < 0.2f)
            {
                CoreHPCol.color = new Color32(165, 69, 25, 255);
            }
            else if (CoreHP.value < 0.5f)
            {
                CoreHPCol.color = new Color32(207, 139, 29, 255);
            }
            else if (CoreHP.value < 0.75f)
            {
                CoreHPCol.color = new Color32(30, 163, 211, 255);
            }
            else
            {
                CoreHPCol.color = new Color32(174, 190, 23, 255);
            }

            BotScrap.text = $"{lb.Metadata.Scrap:0.#}";
            BotScrapD.text = $"x{lb.Metadata.ScrapConversion:0.#}";

            BotMen.text = $"{lb.PeopleFixing}";
            BotMenD.text = $"x{lb.Metadata.RepairRate:0.#}";

            BotFood.text = $"{lb.Metadata.Food}";
            BotFoodD.text = $"-{lb.PeopleFixing + (lb.Player != null ? lb.Player.People : 0)}";

            BotGunName.text = "Plasma cannon";
            BotGunStatus.text = lb.Bot.Gun1.Loaded ? "LOADED" : $"LOADING: {lb.Bot.Gun1.LoadTimeLeft}";
            BotGunStatus.color = lb.Bot.Gun1.Loaded ? new Color32(174, 190, 23, 255) : new Color32(207, 139, 29, 255);

            BotEngineStatus.text = lb.Bot.Engine.Loaded ? "READY" : $"COOLING: {lb.Bot.Engine.LoadTimeLeft}";
            BotEngineStatus.color = lb.Bot.Engine.Loaded ? new Color32(174, 190, 23, 255) : new Color32(207, 139, 29, 255);

            Turn.text = lb.TurnsPassed.ToString();
        }

        PartyPeople.value = lb.Player != null ? lb.Player.People : 0;

        if (lb.Player != null)
        {
            PartyRadio.text = $"{lb.Player.Radio}";
            PartyScrap.text = $"{lb.Player.Scrap}";
            PartyFood.text = $"{lb.Player.Food}";
        }
        else
        {
            PartyRadio.text = "-";
            PartyScrap.text = "-";
            PartyFood.text = "-";
        }

        while (logs.Count > 10)
        {
            logs.Dequeue();
        }
        BotLog.text = "";
        foreach (var l in logs)
        {
            BotLog.text += $"{l}\n";
        }

        foreach (var p in LogCooldowns)
        {
            
        }
        List<string> keys = new List<string>(LogCooldowns.Keys);
        foreach (string key in keys)
        {
            if (LogCooldowns[key]>= 0)
            {
                LogCooldowns[key] -= Time.deltaTime;
            }
        }
    }

    private void LogPhrase(string name, object arg)
    {
        switch (name)
        {
            case "lvlStart":
                logs.Clear();
                Enq($"<color=#FFF>*** Level {LevelBrain.Instance.Metadata.CurrentLevel} started ***</color>");
                GetLevelMessage(LevelBrain.Instance.Metadata.CurrentLevel);
                break;
            case "warning":
                if (CheckLogsCD(name, 3))
                    Enq(Helpers.Pick(new[] {
                    "<color=#d87a3c>*** WARNING! ENEMIES INCOMING! ***</color>",
                    "<color=#d87a3c>21> Warning! I'm detecting large number of enemies.</color>",
                    "<color=#d87a3c>21> Huge wave of boogies incoming. Return immediately.</color>",
                }));
              
                break;
            case "EngineNotReady":
                if (CheckLogsCD(name, 3))
                    Enq(Helpers.Pick(new[] {
                    "21> Unable to leave. Engines not operational yet.",
                    "21> Still waiting for engines to cool down.",
                    "21> Engines cooling, cap. Hold on a little longer.",
                }));
                break;
            case "NoLeaveEmpty":
                if (CheckLogsCD(name, 3))
                    Enq(Helpers.Pick(new[] {
                    "21> Sorry, I can't leave you behind.",
                    "21> I am unable to move without people on board.",
                    "21> I'll wait for you to return. Crew is required to be on board.",
                    "21> Please return before leaving.",
                }));
                break;
            case "enemyNearOutRange":
                if (CheckLogsCD("warn1", 10))
                    Enq(Helpers.Pick(new[] {
                    "21> Be careful. I can't cover you at that range.",
                    "21> Threat detected. You are out of firing range, please return.",
                    "21> Search party, you are in danger. Seek cover or return immediately.",
                    "21> Can't cover your back, Party 1. Be careful."
                }));
                break;
            case "enemyNear":
                if (CheckLogsCD("warn1", 10))
                    Enq(Helpers.Pick(new[] {
                    "21> Threat detected. Acquiring lock.",
                    "21> Hold on, Party 1. I'll cover you.",
                    "21> Guys, keep your heads low, plasma incoming.",
                    "21> Acquiring lock on target near your position.",
                }));
                break;
            case "enemyNearNoAmmo":
                if (CheckLogsCD("warn1", 10))
                    Enq(Helpers.Pick(new[] {
                    "21> Cannon is reloading. Seek cover.",
                    "21> Threat detected, Party 1. I can't cover you until reloaded.",
                    "21> Unable to fire - reloading.",
                    "21> Evade threat, I'm still reloading.",
                }));
                break;
            case "BotAttacked":
                if (CheckLogsCD(name, 15))
                    Enq(Helpers.Pick(new[] {
                    "21> Detecting minor damage. Just a scratch.",
                    "21> Paint scrapped starboard.",
                    "21> Damage control measures applied",
                    "21> Rough.",
                    "21> Ouch.",
                    "21> I'll make you pay for this.",
                }));
                break;
            case "PlayerAttacked":
                if (CheckLogsCD(name, 10))
                    Enq(Helpers.Pick(new[] {
                    "21> You ok Party 1? Party 1?",
                    "21> Careful. Retreat immediately.",
                    "21> Party 1, boogie on your position. Take care.",
                    "21> Oh no",
                }));
                break;
            case "PlayerAttackedInCover":
                if (CheckLogsCD(name, 15))
                    Enq(Helpers.Pick(new[] {
                    "21> Run, while it's distracted!",
                    "21> Your cover is blown, party 1! Retreat!",
                    "21> Everyone is ok? Get back now."
                }));
                break;
            case "PlayerTargeted":
                if (CheckLogsCD("warn2", 25))
                    Enq(Helpers.Pick(new[] {
                    "21> Party 1, enemy detected your position!",
                    "21> Careful, they can see you!"
                }));
                break;
            case "BotTargeted":
                if (CheckLogsCD("warn2", 25))
                    Enq(Helpers.Pick(new[] {
                    "21> They are on me. Acquiring lock.",
                    "21> I've been detected."
                }));
                break;
            case "ShotHeard":
                if (CheckLogsCD(name, 35))
                    Enq(Helpers.Pick(new[] {
                    "21> These things will react to the shots, be careful",
                    "21> Oops, did I wake you up?"
                }));
                break;
            case "enemyKilled":
                if (CheckLogsCD(name, 10))
                    Enq(Helpers.Pick(new[] {
                    "21> Got one.",
                    "21> Enemy down.",
                    "21> Minus one",
                    "21> Get wrecked.",
                    "21> Eat my charged ions.",
                    "21> Huge mess confirmed.",
                }));
                break;
            case "enemyDamaged":
                if (CheckLogsCD(name, 10))
                    Enq(Helpers.Pick(new[] {
                    "21> Enemy still holding.",
                    "21> It is still alive!",
                    "21> Wounded it",
                    "21> Would you die already?",
                }));
                break;
            case "RepairBotDone":
                if (CheckLogsCD(name, 30))
                    Enq(Helpers.Pick(new[] {
                    "21> Repairs are ongoing.",
                    "21> Confirm armor recovering",
                    "21> Scrap applied.",
                    "21> Feel a bit better now, thank you",
                    "21> Thank you for the repairs.",
                }));
                break;
            case "PlayerDead":
                if (CheckLogsCD(name, 10))
                    Enq(Helpers.Pick(new[] {
                    "21> I lost signal of Party 1",
                    "21> Party signal lost. I repeat, party signal lost.",
                    "21> Oh no.",
                    "21> ...",
                    "21> I lost them.",
                }));
                break;
            case "NeedLeave":
                if (CheckLogsCD(name, 30))
                    Enq(Helpers.Pick(new[] {
                    "21> We need to leave.",
                    "21> I advice we save people on board and leave.",
                    "21> We have to leave.",
                    "21> Nothing we can do now."
                }));
                break;
            case "BotDead":
                if (CheckLogsCD(name, 30))
                    Enq(Helpers.Pick(new[] {
                    "21> .system.... off.. line.",
                    "21> Core functions lost. Destruction imminent",
                    "21> Sorry. It seems I can't keep myself together.",
                    "21> Damage critical. Shutting down in 3.. 2... "
                }));
                break;
            case "FoodLow":
                Enq("<color=#d87a3c>(!) Some people left because of the food shortages </color>");
                break;
            case "Lost":
                Enq("Game over");
                break;
            case "LastSequence1":
                Enq("21> Party 1, I confirm your location. Transmitting access codes.");
                break;
            case "LastSequence2":
                Enq("21> Core status critical.");
                break;
            case "LastSequence3":
                Enq("21> Log record. Final mission.");
                break;
            case "LastSequence4":
                Enq("21> Completed successfully.");
                break;
            case "LastSequence5":
                Enq("21> For the sake of humanity. You must keep it alive.");
                break;
        }
    }

    private void GetLevelMessage(int currentLevel)
    {
        switch(currentLevel)
        {
            case 0:
                Enq("21> Priority 1: survive. Priority 2: collect resources and save more people");
                break;
            case 1:
                Enq("21> Damage to the core registered.");
                Enq("21> I've got rusty. Prolonged thruster causes damage to the infrastructure. Some repairs advised");
                break;
            case 3:
                Enq("21> This mall should have a lot of food inside. But be careful.");
                break;
            case 4:
                Enq("21> I detect a high amount of useful materials in those warehouses.");
                break;
            case 6:
                Enq("21> Fast variety detected. Caution is advised.");
                break;
            case 7:
                Enq("21> Armoured enemy detected.");
                break;
            case 10:
                Enq("21> Thrusters offline. Scanning damage.");
                Enq("21> Field repair impossible.");
                Enq("21> Base Condor is up ahead. Go. You will be safe there.");
                break;

        }
    }

    private void Enq(string data)
    {
        logs.Enqueue(data + "\n<line-height=120%> </line-height>");
        scroll.velocity = new Vector2(0f, 1000f);
    }

    private bool CheckLogsCD(string name, float val)
    {
        if (!LogCooldowns.ContainsKey(name))
            LogCooldowns[name] = 0;
        if (LogCooldowns[name] > 0) return false;
        LogCooldowns[name] = val;
        return true;
    }

    private void LogDebug()
    {
        if (LevelBrain.Instance.Bot != null && LevelBrain.Instance.Player != null)
        {
            BotLog.text = $@"
BOT
Health: {LevelBrain.Instance.Bot.Health}
Repair people: {LevelBrain.Instance.PeopleFixing}
Scrap: {LevelBrain.Instance.Metadata.Scrap}
Food: {LevelBrain.Instance.Metadata.Food}

Party
People: {LevelBrain.Instance.Player.People}
Scrap: {LevelBrain.Instance.Player.Scrap}
Food: {LevelBrain.Instance.Player.Food}

Turns: {LevelBrain.Instance.TurnsPassed}
";
        }
    }
}
