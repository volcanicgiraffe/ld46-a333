﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioCollection : MonoBehaviour
{
    public AudioClip explosion1;
    public AudioClip fire1;
    public AudioClip fire2;
    public AudioClip fire3;
    public AudioClip flyIn;
    public AudioClip flyOut;

    public AudioClip enemyAttack;
    public AudioClip enemyDeath;
    public AudioClip enemyShortWalk;
    public AudioClip enemyShortWalk2;
    public AudioClip enemyLongWalk;

    public AudioClip pickup;
    public AudioClip start;
    public AudioClip walk1;
    public AudioClip walk2;
    public AudioClip walk3;
    public AudioClip unable;
    public AudioClip warning1;
    public AudioClip warning2;

    private AudioSource aso;

    private void Start()
    {
        aso = GetComponent<AudioSource>();
    }

    public void Talk(int amnt)
    {
        StartCoroutine(TalkC(amnt));
    }
    IEnumerator TalkC(int amnt)
    {
        for (int i = 0; i < amnt; i++)
        {
            aso.PlayOneShot(pickup);
            yield return new WaitForSeconds(0.1f);
        }
    }
    public void Play(string name)
    {
        switch (name)
        {
            case "shortwalk":
                aso.PlayOneShot(Random.value > 0.5f ? enemyShortWalk : enemyShortWalk2);
                break;
            case "longwalk":
                aso.PlayOneShot(enemyLongWalk);
                break;
            case "enemyattack":
                aso.PlayOneShot(enemyAttack);
                break;
            case "enemydeath":
                aso.PlayOneShot(enemyDeath, 0.5f);
                break;

            case "explosion":
                aso.PlayOneShot(explosion1);
                break;
            case "fire":
                aso.PlayOneShot(fire3, 0.5f);
                break;
            case "flyin":
                aso.PlayOneShot(flyIn, 0.3f);
                break;
            case "flyout":
                aso.PlayOneShot(flyOut, 0.3f);
                break;
            case "pickup":
                aso.PlayOneShot(pickup, 0.7f);
                break;
            case "warning":
                aso.PlayOneShot(warning2);
                break;
            case "walk":
                aso.PlayOneShot(Helpers.Pick(new[] { walk1, walk2, walk3 }));
                break;
            case "unable":
                aso.PlayOneShot(unable);
                break;
            
            case "fire2":
                aso.PlayOneShot(fire2, 0.5f);
                break;
            case "start":
                aso.PlayOneShot(start);
                break;
        }
    }
}
