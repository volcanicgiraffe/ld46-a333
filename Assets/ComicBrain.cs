﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;

public class ComicBrain : MonoBehaviour
{
    public SpriteRenderer[] Frames;
    int current;
    bool mouseDown;
    private int currentTween;

    AudioSource aso;
    public AudioClip top;
    public AudioClip att;
    public AudioClip warn;
    public AudioClip fire;
    public AudioClip death;
    public AudioClip talk;
    public AudioClip explosion;
    public AudioClip fly;

    public bool Intro = true;

    void Start()
    {
        aso = GetComponent<AudioSource>();
        var ls = new List<SpriteRenderer>();
        foreach (var sr in Frames)
        {
            sr.DOFade(0, 0);
            ls.Add(sr);
        }
        ls.Reverse();
        Frames = ls.ToArray();

        if (Intro)
        {
            StartCoroutine(PlayComic1());
        }
        else
        {
            StartCoroutine(PlayComic2());
        }

    }

    IEnumerator Audio1()
    {
        aso.PlayOneShot(att);
        yield return Wait(0.4f);
        aso.PlayOneShot(att);
    }

    IEnumerator AfterDelayPlay(float delay, AudioClip clp, float volume = 1)
    {
        yield return Wait(delay);
        aso.PlayOneShot(clp, volume);
    }

    IEnumerator Audio2(int amnt, float wt)
    {
        for (int i = 0; i < amnt; i++)
        {
            aso.PlayOneShot(talk);
            yield return Wait(wt);
        }
    }

    IEnumerator PlayComic1()
    {
        yield return PlayFrame(0, 0.2f, 2f);
        aso.PlayOneShot(top);
        yield return PlayFrame(1, 2f, 0.1f);
        yield return PlayFrame(2, 0.2f, 1f);
        StartCoroutine(Audio1());
        yield return PlayFrame(3, 0.2f, 0.3f);
        yield return PlayFrame(4, 0.2f, 3f);
        HideAll();

        StartCoroutine(AfterDelayPlay(0.5f, warn));
        yield return PlayFrame(5, 0.2f, 3f);
        aso.PlayOneShot(fire, 0.1f);
        yield return PlayFrame(6, 0.2f, 0.1f);
        Frames[6].DOFade(0, 0);
        Frames[5].DOFade(0, 0);
        aso.PlayOneShot(death);
        yield return PlayFrame(7, 0f, 3f);
        Frames[7].DOFade(0, 0);
        yield return PlayFrame(8, 0.2f, 4f);
        Frames[8].DOFade(0, 0);

        yield return PlayFrame(9, 0.2f, 1f);
        StartCoroutine(Audio2(3, 0.1f));
        yield return PlayFrame(10, 0.2f, 2f);
        StartCoroutine(Audio2(3, 0.1f));
        yield return PlayFrame(11, 0.2f, 2f);
        StartCoroutine(Audio2(5, 0.1f));
        yield return PlayFrame(12, 0.2f, 4f);
        HideAll();

        yield return PlayFrame(13, 0.2f, 1f);
        StartCoroutine(Audio2(6, 0.1f));
        yield return PlayFrame(15, 0.2f, 2f);
        aso.PlayOneShot(fly, 0.1f);
        yield return PlayFrame(14, 0.2f, 2f);
        StartCoroutine(Audio2(6, 0.1f));
        yield return PlayFrame(16, 0.2f, 4f);
        HideAll();

        UnityEngine.SceneManagement.SceneManager.LoadScene("Level");
    }
    IEnumerator PlayComic2()
    {
        yield return PlayFrame(0, 0.2f, 4f);
        yield return PlayFrame(1, 0.1f, 0.5f);
        StartCoroutine(AfterDelayPlay(0.3f, explosion, 0.2f));
        Camera.main.DOShakePosition(0.1f, 0.5f).SetDelay(0.3f);
        yield return new WaitForSeconds(0.5f);
        Frames[1].DOFade(0, 0.8f);
        Frames[0].DOFade(0, 0.8f);
        
        yield return PlayFrame(2, 0.2f, 6f);
        yield return Frames[2].transform.DOMoveY(-18, 10).SetEase(Ease.InOutCubic).WaitForCompletion();
        yield return new WaitForSeconds(3f);
        UnityEngine.SceneManagement.SceneManager.LoadScene("Win");
    }


    // Update is called once per frame
    void Update()
    {
        mouseDown = false;
        if (Intro && Input.GetKeyDown(KeyCode.R))
        {
            DOTween.CompleteAll();
            StopAllCoroutines();
            HideAll();
            StartCoroutine(PlayComic1());
        }
        if (Intro && (Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(0)))
        {
            mouseDown = true;
            DOTween.CompleteAll();
        }
    }

    private void HideAll()
    {
        foreach (var sr in Frames)
        {
            sr.DOFade(0, 0);
        }
    }

    IEnumerator PlayFrame(int num, float fade, float wait)
    {
        yield return Frames[num].DOFade(1, fade).WaitForCompletion();
        yield return Wait(wait);
    }

    IEnumerator Wait(float time)
    {
        for (float timer = time; timer >= 0; timer -= Time.deltaTime)
        {
            if (mouseDown)
            {
                yield break;
            }
            yield return null;
        }
    }
}
