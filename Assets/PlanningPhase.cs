﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanningPhase : MonoBehaviour, IPhase
{
    public bool completed { get; set; }

    // Start is called before the first frame update
    void Start()
    {

    }

    public void InitData()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void ShowFoodDialog()
    {
    }
    private void ShowPlanDialog()
    {
        var md = LevelBrain.Instance.Metadata;
        if (LevelBrain.Instance.CurrentLevel.Final)
        {
            LevelBrain.Instance.PeopleFixing = 0;
            LevelBrain.Instance.Player.People = md.PeopleOnboard;
        }
        else
        {
            LevelBrain.Instance.PeopleFixing = md.PeopleOnboard / 2;
            LevelBrain.Instance.Player.People = md.PeopleOnboard - LevelBrain.Instance.PeopleFixing;
        }

    }

    public void OnEnable()
    {
        var md = LevelBrain.Instance.Metadata;
        if (md.Food < md.PeopleOnboard)
        {
            EventManager.Trigger("FoodLow");
            ShowFoodDialog();
            md.PeopleOnboard = Mathf.Max(md.Food, 1);
        }
        md.Food -= md.PeopleOnboard;
        md.Food = Mathf.Max(md.Food, 0);

        ShowPlanDialog();

        completed = true;
        Debug.Log("Planning phase complete");
    }



    public void OnDisable()
    {

    }
}
