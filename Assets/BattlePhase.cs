﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Tilemaps;
using DG.Tweening;

public enum Turn
{
    Player, Bot, Enemies
}


public class BattlePhase : MonoBehaviour, IPhase
{
    public GameObject TileCursor;
    public Turn turn = Turn.Enemies;
    public Tilemap GroundTileMap;
    bool playingAnims = false;
    private Player Player;
    private RangeHighlighter HLArea;
    private RangeHighlighter HLPath;
    private RangeHighlighter HLDanger;
    private Bot Bot;

    private List<Vector3Int> hPath;
    private Vector3Int oldHCell;
    private List<Vector3Int> incomingSpawns = new List<Vector3Int>();

    public bool completed { get; set; }

    public void Update()
    {
        if (playingAnims) return;

        switch (turn)
        {
            case Turn.Player:
                HandlePlayerInput();
                break;
            case Turn.Enemies:
                break;
            case Turn.Bot:
                break;
        }
    }

    private IEnumerator FindEnemyAndAttack()
    {

        SortedList<float, GameObject> threats = new SortedList<float, GameObject>();
        bool enemyNear = false;
        bool enemyNearOutRange = false;
        HLArea.ShowRadius(CellPos(Bot), LevelBrain.Instance.CurrentLevel.Width, LevelBrain.Instance.CurrentLevel.Height, Bot.FireRange, false, new Color(240f / 255f, 174f / 255f, 46f / 255f));
        foreach (var enemy in LevelBrain.Instance.Enemies)
        {
            var playerDist = Vector3.Distance(new Vector3(enemy.transform.position.x, enemy.transform.position.y, 0), new Vector3(Player.transform.position.x, Player.transform.position.y, 0));
            if (playerDist == 0) playerDist = 0.1f;
            var botDist = Vector3.Distance(new Vector3(enemy.transform.position.x, enemy.transform.position.y, 0), new Vector3(Bot.transform.position.x, Bot.transform.position.y, 0));
            if (botDist == 0) botDist = 0.1f;
            var coeff = (1 / playerDist) * 3 + (1 / botDist);
            if (playerDist < 4) enemyNear = true;
            if (HLArea.InRange(CellPos(enemy)))
            {
                if (!threats.ContainsKey(coeff))
                    threats.Add(coeff, enemy.gameObject);
            }
            else
            {
                if (playerDist < 4) enemyNearOutRange = true;
            }
        }
        if (enemyNearOutRange)
        {
            EventManager.Trigger("enemyNearOutRange");
        }
        else if (enemyNear)
        {
            EventManager.Trigger("enemyNear");
        }
        if (Bot.Gun1.Loaded)
        {
            if (threats.Count > 0)
            {
                yield return new WaitForSeconds(0.5f);
                yield return BotFire(threats.Values[threats.Values.Count - 1]);
            }
        }
        else
        {
            Bot.Gun1.Reload();
            if (threats.Count > 0 && enemyNear)
            {
                EventManager.Trigger("enemyNearNoAmmo");
            }
        }
        Bot.Engine.Reload();
        yield return new WaitForEndOfFrame();
        SwitchTurn();
    }

    private IEnumerator EnemyActions()
    {
        yield return new WaitForSeconds(0.3f);
        var cpBot = CellPos(Bot);
        var cpPlayer = CellPos(Player);
        foreach (var enemy in LevelBrain.Instance.Enemies)
        {
            var cpEnemy = CellPos(enemy);
            var area = PathFind.Pathfinding.FindArea(LevelBrain.Instance.AStarGrid, cpEnemy, enemy.Range);
            HLArea.ShowAll(area, LevelBrain.Instance.CurrentLevel.Width, LevelBrain.Instance.CurrentLevel.Height, new Color(230f / 255f, 204f / 255f, 46f / 255f));
            if (LevelBrain.Instance.Enemies.Count < 5) yield return new WaitForSeconds(0.3f);
            //Debug.Log("After wait");
            if (cpEnemy.ChebyshevDistance(cpBot) <= 1)
            {
                yield return AttackTween(enemy, Bot.gameObject);
                EventManager.Trigger("BotAttacked");
                Bot.Health -= enemy.Damage;
            }
            else if (cpEnemy.ChebyshevDistance(cpPlayer) <= 1)
            {
                yield return AttackTween(enemy, Player.gameObject);
                if (LevelBrain.Instance.GroundObjects.ContainsKey(cpPlayer))
                {
                    LevelBrain.Instance.Sound.Play("fire");
                    if (LevelBrain.Instance.GroundObjects[cpPlayer].GetComponent<GroundObject>().type != GroundObjectsType.Base)
                    {
                        Destroy(LevelBrain.Instance.GroundObjects[cpPlayer]);
                        EventManager.Trigger("PlayerAttackedInCover");
                    }

                }
                else
                {
                    EventManager.Trigger("PlayerAttacked");
                    Player.People -= 1;
                    LevelBrain.Instance.Metadata.TotalPeopleDied++;
                }
            }
            else if (cpEnemy.ManhattanDistance(cpPlayer) <= enemy.PlayerDetectRange)
            {
                EventManager.Trigger("PlayerTargeted");
                yield return TryMoveTo(enemy, cpPlayer);
            }
            else if (cpEnemy.ManhattanDistance(cpBot) <= enemy.BotDetectRange)
            {
                EventManager.Trigger("BotTargeted");
                yield return TryMoveTo(enemy, cpBot);
            }
            else if (enemy.heardTarget != null)
            {
                yield return TryMoveTo(enemy, CellPos(enemy.heardTarget));
            }
            else if (UnityEngine.Random.value < enemy.StandChance)
            {
                var list = new List<Vector3Int>();
                foreach (var key in HLArea.Ranges.Keys)
                {
                    list.Add(key);
                }
                if (list.Count > 0)
                {
                    var path = PathFind.Pathfinding.FindPath(LevelBrain.Instance.AStarGrid, cpEnemy, list[UnityEngine.Random.Range(0, list.Count - 1)]);
                    if (path != null && path.Count != 0)
                    {
                        if (LevelBrain.Instance.Enemies.Count < 10) yield return Move(enemy.gameObject, path);
                        else Move(enemy.gameObject, path);
                    }

                }
            }
        }

        //Debug.Log("End enemy actions");
        SwitchTurn();
    }

    private IEnumerator AttackTween(Enemy enemy, GameObject target)
    {
        LevelBrain.Instance.Sound.Play("enemyattack");
        var back = new Vector3(enemy.transform.position.x, enemy.transform.position.y, enemy.transform.position.z);
        var cf = 1 - 0.5f / (target.transform.position - back).magnitude;
        var halfMetreBeforeTarget = back + (target.transform.position - back) * cf;

        yield return enemy.transform.DOMove(halfMetreBeforeTarget, 0.1f).SetEase(Ease.Linear).WaitForCompletion();
        target.transform.DOShakePosition(0.3f, 0.1f);
        yield return enemy.transform.DOMove(back, 0.4f).SetEase(Ease.OutCubic).WaitForCompletion();

    }

    private IEnumerator TryMoveTo(Enemy enemy, Vector3Int target)
    {
        //Debug.Log("TryMoveTo " + CellPos(enemy).ToString());
        var path = PathFind.Pathfinding.FindPath(LevelBrain.Instance.AStarGrid, CellPos(enemy), target);

        if (path != null && path.Count != 0)
        {
            var subPath = path.Count >= enemy.Range ? path.GetRange(0, enemy.Range) : path;
            if (subPath.Count == path.Count)
                subPath = subPath.GetRange(0, subPath.Count - 1);
            LevelBrain.Instance.Sound.Play(subPath.Count < 2 ? "shortwalk" : "longwalk");
            yield return Move(enemy.gameObject, subPath, LevelBrain.Instance.Enemies.Count > 8 ? 0.1f : 0.2f);
        }
    }

    private IEnumerator BotFire(GameObject target)
    {
        foreach (var enemy in LevelBrain.Instance.Enemies)
        {
            if (Vector3.Distance(enemy.transform.position, Bot.transform.position) < enemy.ShotDetectRange)
            {
                EventManager.Trigger("ShotHeard");
                enemy.heardTarget = Bot.gameObject;
            }
        }
        OnStartAnim();
        Bot.Gun1.Fire();
        LevelBrain.Instance.Sound.Play("fire");
        var obj = Instantiate(Bot.Projectile);
        obj.transform.SetParent(LevelBrain.Instance.MapContainer.transform, false);
        obj.transform.position = new Vector3(Bot.transform.position.x, Bot.transform.position.y - 0.2f, 0);
        yield return obj.transform.DOMove(target.transform.position, 0.2f).SetEase(Ease.Linear).WaitForCompletion();
        var e = target.GetComponent<Enemy>();
        Destroy(obj);
        e.Health -= 1;
        if (e.Health <= 0)
        {
            LevelBrain.Instance.Metadata.TotalKilled++;
            LevelBrain.Instance.Sound.Play("enemydeath");
            EventManager.Trigger("enemyKilled");
        }
        else
        {
            EventManager.Trigger("enemyDamaged");
        }
    }

    private void HandlePlayerInput()
    {
        if (Player == null) return;
        if (Input.GetKeyDown(KeyCode.Space)) { SwitchTurn(); }
        if (Input.GetKeyDown(KeyCode.E))
        {
            Leave();
        }
        var mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        var hCell = GroundTileMap.WorldToCell(new Vector3(mouseWorldPos.x + 0.5f * LevelBrain.Instance.MapContainer.transform.localScale.x, mouseWorldPos.y, 0));
        //UI.Instance.BotLog.text = $"mopo.x: {mouseWorldPos.x},\n cellSize: {GroundTileMap.cellSize.x},\n locSc: {LevelBrain.Instance.MapContainer.transform.localScale.x}, hc: {hCell.x} ";
        if (hCell.x >= 0 && hCell.y >= 0 && hCell.x < LevelBrain.Instance.CurrentLevel.Width
            && hCell.y < LevelBrain.Instance.CurrentLevel.Height)
        {
            if (hCell != oldHCell)
            {
                TileCursor.transform.position = GetTileBasedPos(hCell);
                hPath = PathFind.Pathfinding.FindPath(LevelBrain.Instance.AStarGrid, GroundTileMap.WorldToCell(Player.transform.position), hCell);
                oldHCell = hCell;
                if (hCell == CellPos(Bot))
                {
                    HLPath.ShowRadius(CellPos(Bot), LevelBrain.Instance.CurrentLevel.Width, LevelBrain.Instance.CurrentLevel.Height, Bot.FireRange, false, new Color(240f / 255f, 174f / 255f, 46f / 255f));
                }
                else
                {
                    HLPath.Hide();
                    if (HLArea.InRange(hCell))
                    {
                        HLPath.ShowAll(hPath, LevelBrain.Instance.CurrentLevel.Width, LevelBrain.Instance.CurrentLevel.Height, new Color(40f / 255f, 234f / 255f, 146f / 255f));
                    }
                }

                GameObject hObj = null;
                LevelBrain.Instance.GroundObjects.TryGetValue(hCell, out hObj);
                if (hObj != null)
                {

                }
            }
            if (Input.GetMouseButtonDown(1) || Input.GetMouseButtonDown(0))
            {
                if (HLArea.InRange(hCell))
                {
                    Player.canWalkDiagonally = !Player.canWalkDiagonally;
                    LevelBrain.Instance.Sound.Play("walk");
                    StartCoroutine(MovePlayer(Player.gameObject, hPath));
                }
                else
                {
                    LevelBrain.Instance.Sound.Play("unable");
                }
            }
        }
    }

    public void Leave()
    {
        if (!Bot.Engine.Loaded)
        {
            EventManager.Trigger("EngineNotReady");
            LevelBrain.Instance.Sound.Play("unable");
        }
        else if (LevelBrain.Instance.PeopleFixing == 0 && CellPos(Player) != CellPos(Bot))
        {
            EventManager.Trigger("NoLeaveEmpty");
        }
        else
        {
            completed = true;
        }
    }

    private IEnumerator MovePlayer(GameObject player, List<Vector3Int> path)
    {
        yield return Move(player, path);
        SwitchTurn();
    }

    private IEnumerator Move(GameObject gameObject, List<Vector3Int> path, float time = 0.2f)
    {
        //Debug.Log("Move " + CellPos(gameObject).ToString() + " path.count: " + path.Count);
        if (path.Count != 0)
        {
            OnStartAnim();
            var path2 = new List<Vector3>();
            foreach (var p in path) { path2.Add(p); }
            yield return gameObject.transform.DOLocalPath(path2.ToArray(), path.Count * time, PathType.Linear, PathMode.Ignore).WaitForCompletion();
        }
    }

    void OnStartAnim()
    {
        playingAnims = true;
        HLArea.Hide();
        HLPath.Hide();
    }


    private Vector3 GetTileBasedPos(Vector3Int pos)
    {
        var pos1 = GroundTileMap.CellToWorld(pos);
        return new Vector3(pos1.x, pos1.y, 0);
    }

    public void SwitchTurn()
    {
        playingAnims = false;
        HLArea.Hide();
        HLPath.Hide();
        switch (turn)
        {
            case Turn.Player:
                turn = Turn.Bot;
                Debug.Log("Bot's turn");

                RepairBot();
                StartCoroutine(FindEnemyAndAttack());
                TileCursor.SetActive(false);
                break;
            case Turn.Bot:
                turn = Turn.Enemies;
                Debug.Log("Enemies turn");
                LevelBrain.Instance.TurnsPassed++;
                LevelBrain.Instance.Metadata.TotalTurns++;
                if (LevelBrain.Instance.TurnsPassed == LevelBrain.Instance.CurrentLevel.TurnsUntilSD)
                {
                    LevelBrain.Instance.Sound.Play("warning");
                    EventManager.Trigger("warning");
                }
                if (LevelBrain.Instance.TurnsPassed > LevelBrain.Instance.CurrentLevel.TurnsUntilSD)
                {
                    LevelBrain.Instance.AddEnemies(incomingSpawns);
                    incomingSpawns = LevelBrain.Instance.GetSpawnPositions();
                    HLDanger.ShowAll(incomingSpawns, LevelBrain.Instance.CurrentLevel.Width, LevelBrain.Instance.CurrentLevel.Height, new Color(240f / 255f, 104f / 255f, 46f / 255f));
                }

                HLArea.Hide();
                HLPath.Hide();
                TileCursor.SetActive(false);
                StartCoroutine(EnemyActions());
                break;
            case Turn.Enemies:
                turn = Turn.Player;
                Debug.Log("Player turn");
                if (Player == null) return;
                TileCursor.SetActive(true);
                var area = PathFind.Pathfinding.FindArea(LevelBrain.Instance.AStarGrid, CellPos(Player), Player.Range, Player.canWalkDiagonally);
                HLArea.ShowAll(area, LevelBrain.Instance.CurrentLevel.Width, LevelBrain.Instance.CurrentLevel.Height, new Color(230f / 255f, 204f / 255f, 46f / 255f));
                break;
        }
    }

    private void RepairBot()
    {
        if (Bot.Health < 100)
        {
            var scrapNeeded = (100 - Bot.Health) / LevelBrain.Instance.Metadata.ScrapConversion;
            var scrapCanGet = Math.Min(LevelBrain.Instance.PeopleFixing * LevelBrain.Instance.Metadata.RepairRate, LevelBrain.Instance.Metadata.Scrap);
            EventManager.Trigger("RepairBot", new float[] { scrapNeeded, scrapCanGet });
            var scrap = Math.Min(scrapNeeded, scrapCanGet);
            Bot.Health += scrap * LevelBrain.Instance.Metadata.ScrapConversion;
            if (scrap * LevelBrain.Instance.Metadata.ScrapConversion > 0)
                EventManager.Trigger("RepairBotDone");
            LevelBrain.Instance.Metadata.Scrap -= scrap;
        }
    }

    private Vector3Int CellPos(GameObject obj)
    {
        return GroundTileMap.WorldToCell(obj.transform.position);
    }

    private Vector3Int CellPos(MonoBehaviour obj)
    {
        return GroundTileMap.WorldToCell(obj.transform.position);
    }
    public void OnEnable()
    {
        TileCursor.SetActive(true);
        turn = Turn.Enemies;
        GroundTileMap = LevelBrain.Instance.GroundTileMap;
        Player = LevelBrain.Instance.Player;
        var c = LevelBrain.Instance.MapContainer.GetComponents<RangeHighlighter>();
        HLArea = c[0];
        HLPath = c[1];
        HLDanger = c[2];
        Bot = LevelBrain.Instance.Bot;
        hPath = new List<Vector3Int>();
        oldHCell = Vector3Int.zero;
        incomingSpawns = new List<Vector3Int>();
        SwitchTurn();
    }

    public void OnDisable()
    {
        if (TileCursor != null) TileCursor.SetActive(false);
        HLArea.Hide();
        HLDanger.Hide();
        HLPath.Hide();
        StopAllCoroutines();
    }
}

