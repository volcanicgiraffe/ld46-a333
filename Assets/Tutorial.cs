﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    public GameObject[] tuts;
    private int index;
    public Button ok;
    public Button cont;
    // Start is called before the first frame update
    void Start()
    {

    }


    public void ShowNext()
    {
        if (index > 0)
            tuts[index - 1].SetActive(false);
        if (index >= tuts.Length)
        {
            Done();
        }
        else
        {
            tuts[index].SetActive(true);
            index++;
        }
    }

    void OnEnable()
    {
        ok.gameObject.SetActive(true);
        cont.gameObject.SetActive(true);
    }
    public void Done()
    {
        ok.gameObject.SetActive(false);
        cont.gameObject.SetActive(false);
        gameObject.SetActive(false);
    }
}
