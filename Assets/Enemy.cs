﻿using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public Sprite[] EnemySprites = new Sprite[0];
    private Sprite currSprite;
    public float Health = 1;
    public float Damage = 2;
    public int Range = 2;
    public float StandChance = 0.3f;
    public int BotDetectRange = 10;
    public int ShotDetectRange = 15;
    public int PlayerDetectRange = 3;
    public EnemyType Type;
    private SpriteRenderer sr;
    public GameObject heardTarget;
    public bool Dead;

    public void LoadData(EnemyType enType)
    {
        Type = enType;
        sr = GetComponent<SpriteRenderer>();
        switch (enType)
        {
            case EnemyType.Basic:
                sr.sprite = EnemySprites[0];
                currSprite = sr.sprite;
                Health = 1;
                Damage = 2.5f;
                Range = 1;
                StandChance = 0.3f;
                BotDetectRange = 10;
                ShotDetectRange = 15;
                PlayerDetectRange = 3;
                break;
            case EnemyType.Watcher:
                sr.sprite = EnemySprites[1];
                currSprite = sr.sprite;
                Health = 1;
                Damage = 4;
                Range = 1;
                StandChance = 0.6f;
                BotDetectRange = 20;
                ShotDetectRange = 25;
                PlayerDetectRange = 10;
                break;
            case EnemyType.Runner:
                sr.sprite = EnemySprites[2];
                currSprite = sr.sprite;
                Health = 1;
                Damage = 1;
                Range = 3;
                StandChance = 0.1f;
                BotDetectRange = 8;
                ShotDetectRange = 12;
                PlayerDetectRange = 1;
                break;
            case EnemyType.Fatty:
                sr.sprite = EnemySprites[3];
                currSprite = sr.sprite;
                Health = 2;
                Damage = 6;
                Range = 1;
                StandChance = 0.2f;
                BotDetectRange = 10;
                ShotDetectRange = 15;
                PlayerDetectRange = 3;
                break;

        }
    }
    void Start()
    {

    }

    void Update()
    {
        if (Dead) return;
        if (Health <= 0)
        {
            Dead = true;
            GetComponent<Animator>().SetBool("Die", true);
        }
    }

    private void LateUpdate()
    {
        if (Dead) return;
        //https://answers.unity.com/questions/676812/unity-2d-can-you-change-a-2d-animation-clips-sprit.html
        sr.sprite = currSprite;

    }

}
