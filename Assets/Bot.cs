﻿using UnityEngine;
using System.Collections.Generic;
using System;

public class Loadable
{
    public bool Loaded = true;
    public int LoadTime = 1;
    public int LoadTimeLeft = 0;
    public string id;

    public Loadable(string _id)
    {
        id = _id;
    }
    public void Fire()
    {
        Loaded = false;
        LoadTimeLeft = LoadTime;
    }

    public void Reload()
    {
        LoadTimeLeft -= 1;
        if (LoadTimeLeft <= 0)
        {
            LoadTimeLeft = LoadTime;
            Loaded = true;
            EventManager.Trigger("Loaded");
        }
    }
}

public class Bot : MonoBehaviour
{
    public float Health = 100;
    public Loadable Gun1 = new Loadable("Gun1");
    public Loadable Gun2 = new Loadable("Gun2");
    public Loadable Engine = new Loadable("Engine");

    public int FireRange = 6;
    public int MoveRange = 6;
    public GameObject Projectile;
    internal bool flying;

    void Start()
    {


    }

    // Update is called once per frame
    void Update()
    {

    }

   

    public void Fly(bool val)
    {
        GetComponent<Animator>().SetBool("Flying", val);
        flying = val;
    }
    public void Reload()
    {
        Gun1.Reload();
        Engine.Reload();
    }

    internal void LoadData(GameMetadata metadata)
    {
        Health = metadata.BotHealth;
        Engine.LoadTime = 10;
        Engine.LoadTimeLeft = 10;
        Engine.Loaded = false;
    }
}
