﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
public static class Helpers
{
    public static List<Vector3Int> GetTilesNearBorder(this Tilemap tm, int distance, BoundsInt? bounds = null)
    {
        List<Vector3Int> result = new List<Vector3Int>();
        var area = bounds.HasValue ? bounds.Value : tm.cellBounds;
        var block = tm.GetTilesBlock(area);
        for (int i = 0; i < block.Length; i++)
        {
            if (i % area.xMax < distance
                || i % area.xMax >= area.xMax - distance
                || i <= area.xMax * distance
                || i >= area.yMax * area.xMax - area.xMax * distance)
            {
                result.Add(new Vector3Int(i % area.xMax, i / area.xMax, 0));
            }
        }
        return result;
    }

    public static List<Vector3Int> GetTilesExceptNearPoint(this Tilemap tm, Vector3Int point, bool diagonal, int distance, BoundsInt? bounds = null)
    {
        List<Vector3Int> result = new List<Vector3Int>();
        var area = bounds.HasValue ? bounds.Value : tm.cellBounds;
        var block = tm.GetTilesBlock(area);
        for (int i = 0; i < block.Length; i++)
        {
            var vec = new Vector3Int(i % area.xMax, i / area.xMax, 0);
            var dist = diagonal ? point.ChebyshevDistance(vec) : point.ManhattanDistance(vec);
            if (dist >= distance)
            {
                result.Add(vec);
            }
        }
        return result;
    }

    // laziness evolved 
    public static List<Vector3Int> GetTilesNearPoint(this Tilemap tm, Vector3Int point, bool diagonal, int distance, BoundsInt? bounds = null)
    {
        List<Vector3Int> result = new List<Vector3Int>();
        var area = bounds.HasValue ? bounds.Value : tm.cellBounds;
        var block = tm.GetTilesBlock(area);
        for (int i = 0; i < block.Length; i++)
        {
            var vec = new Vector3Int(i % area.xMax, i / area.xMax, 0);
            var dist = diagonal ? point.ChebyshevDistance(vec) : point.ManhattanDistance(vec);
            if (dist < distance)
            {
                result.Add(vec);
            }
        }
        return result;
    }

    public static int ManhattanDistance(this Vector3Int a, Vector3Int b)
    {
        checked
        {
            return Mathf.Abs(a.x - b.x) + Mathf.Abs(a.y - b.y) + Mathf.Abs(a.z - b.z);
        }
    }

    public static int ChebyshevDistance(this Vector3Int a, Vector3Int b)
    {
        checked
        {
            return Mathf.Max(Mathf.Abs(b.x - a.x), Mathf.Abs(b.y - a.y));
        }
    }

    public static T Pick<T>(IEnumerable<T> list)
    {
        var list2 = new List<T>();
        foreach (var i in list)
        {
            list2.Add(i);
        }
        if (list2.Count == 0) return default;
        return list2[UnityEngine.Random.Range(0, list2.Count)];
    }

    public static List<T> PickRange<T>(IEnumerable<T> list, int amount, bool unique = false)
    {
        var list2 = new List<T>();
        foreach (var i in list)
        {
            list2.Add(i);
        }
        if (list2.Count == 0) return default;
        var result = new List<T>();
        for (int i = 0; i < amount; i++)
        {
            var val = list2[UnityEngine.Random.Range(0, list2.Count)];
            if (unique && list2.Count > amount)
            {
                while (result.Contains(val))
                {
                    val = list2[UnityEngine.Random.Range(0, list2.Count)];
                }
            }
            result.Add(val);
        }
        return result;
    }

}
