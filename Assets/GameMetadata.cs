﻿using System;
using UnityEngine;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

[Serializable]
public class GameMetadata
{
    [SerializeField]
    public int CurrentLevel = 0;
    [SerializeField]
    public int PeopleOnboard = 1;
    [SerializeField]
    public float Scrap = 0;
    [SerializeField]
    public int Food = 4;
    [SerializeField]
    public float BotHealth = 100;
    [SerializeField]
    public float RepairRate = 0.7f;
    [SerializeField]
    public float ScrapConversion = 1;
    [SerializeField]
    public bool SeenTutorial = false;


    [SerializeField]
    public int TotalKilled = 0;

    [SerializeField]
    public int TotalTurns = 0;

    [SerializeField]
    public int TotalResources = 0;

    [SerializeField]
    public int TotalSurvivorsSaved = 0;

    [SerializeField]
    public int TotalEndMission = 0;

    [SerializeField]
    public int TotalPeopleDied = 0;

    //[SerializeField]
    //public int TotalRestarts = 0;
}
