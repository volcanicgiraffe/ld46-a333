﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class MenuBrain : MonoBehaviour
{
    public GameObject start;
    public GameObject continueGame;
    public SpriteRenderer bot;
    public SpriteRenderer people;
    public SpriteRenderer logo;
    public SpriteRenderer fader;
    public AudioClip sndWhoosh;
    public AudioClip sndStart;
    AudioSource aus;

    public bool SaveAvailable;

    // Start is called before the first frame update
    void Start()
    {
        aus = GetComponent<AudioSource>();
        StartCoroutine(Animate());
        SaveAvailable = !string.IsNullOrEmpty(PlayerPrefs.GetString("data"));
        continueGame.GetComponent<Button>().interactable = SaveAvailable;
    }

    private IEnumerator Animate()
    {
        var pStPos = people.transform.position;
        people.transform.position = new Vector3(24, -10, 0);
        var botStPos = bot.transform.position;
        bot.transform.position = new Vector3(-29, -9, 0);
        var logoStPos = logo.transform.position;
        logo.transform.position += new Vector3(0, 1);

        yield return new WaitForSeconds(0.5f);

        people.transform.DOMove(pStPos, 1.5f).SetEase(Ease.OutCubic);
        bot.transform.DOMove(botStPos, 1.7f).SetEase(Ease.OutCubic);

        yield return new WaitForSeconds(1.7f);

        logo.DOFade(1, 0.5f);
        yield return logo.transform.DOMove(logoStPos, 0.5f).SetEase(Ease.OutCubic).WaitForCompletion();
        aus.PlayOneShot(sndWhoosh);

        start.SetActive(true);
        continueGame.SetActive(true);

    }

    public void StartGame()
    {
        PlayerPrefs.DeleteAll();
        StartCoroutine(startGameC(true));
    }

    public void Continue()
    {
        StartCoroutine(startGameC());
    }

    IEnumerator startGameC(bool intro = false)
    {
        aus.PlayOneShot(sndStart);
        fader.DOFade(1, 0.5f);
        yield return new WaitForSeconds(1);
        if (intro)
            UnityEngine.SceneManagement.SceneManager.LoadScene("Intro");
        else
            UnityEngine.SceneManagement.SceneManager.LoadScene("Level");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
