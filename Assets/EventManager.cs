﻿using System;
using System.Collections.Generic;
using UnityEngine;

public static class EventManager
{
    public static Dictionary<string, List<Action<string, object>>> Handlers = new Dictionary<string, List<Action<string, object>>>();

    public static void Subscribe(string name, Action<string, object> cb)
    {
        if (!Handlers.ContainsKey(name))
        {
            Handlers[name] = new List<Action<string, object>>();
        }
        Handlers[name].Add(cb);
    }

    public static void Trigger(string name, object data = null)
    {
        try
        {
            var strData = data != null ? data.ToString() : "";
            if (data is Array)
            {
                foreach (var item in (data as Array))
                {
                    strData += " " + item.ToString();
                }
            }
            Debug.Log("EVE: " + name + " " + strData);
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }

        if (Handlers.ContainsKey(name))
        {
            foreach (var cb in Handlers[name])
            {
                cb(name, data);
            }
        }
        if (Handlers.ContainsKey("*"))
        {
            foreach (var cb in Handlers["*"])
            {
                cb(name, data);
            }
        }
    }

}
