﻿using PathFind;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;
using DG.Tweening;

[RequireComponent(typeof(BattlePhase))]
public class LevelBrain : MonoBehaviour
{
    public RandomLevelSeed CurrentLevel;

    public GameObject TutorialContainer;

    public GameObject MapContainer;
    public GameObject BotShadow;

    public Tilemap GroundTileMap;
    public Tilemap FinalMap;
    public GroundTileModel[,] GroundData;

    public GameObject GroundObjectPrefab;
    public Dictionary<Vector3Int, GameObject> GroundObjects = new Dictionary<Vector3Int, GameObject>();

    public GameObject PlayerPrefab;
    public Player Player;

    public GameObject BotPrefab;
    public Bot Bot;

    public GameObject EnemyPrefab;
    public List<Enemy> Enemies = new List<Enemy>();

    public AudioCollection Sound;

    public Tileset Tiles;

    public static LevelBrain Instance;

    public GameMetadata Metadata = new GameMetadata();
    public int PeopleFixing = 0;

    public AStarGrid AStarGrid;

    BattlePhase BattlePhase;
    PlanningPhase PlanningPhase;
    IPhase activePhase;

    HashSet<Vector3Int> usedTiles = new HashSet<Vector3Int>();

    public int TurnsPassed;

    public SpriteRenderer Blast;
    public GameObject GameOverDialog;
    private bool preventUpdate;
    private bool finalSequence;

    void Start()
    {
        Instance = this;
        BattlePhase = GetComponent<BattlePhase>();
        PlanningPhase = GetComponent<PlanningPhase>();

        //fader.DOFade(0, 0f);
        StartCoroutine(StartLevel());
    }

    void Save()
    {
        PlayerPrefs.SetString("data", JsonUtility.ToJson(Metadata));
        PlayerPrefs.Save();
    }
    void Load()
    {
        var data = PlayerPrefs.GetString("data");
        if (!String.IsNullOrEmpty(data))
        {
            Metadata = JsonUtility.FromJson<GameMetadata>(data);
        }
        else
        {
            Metadata = new GameMetadata();
        }
    }


    void Update()
    {
        if (preventUpdate) return;
        if (Input.GetKeyDown(KeyCode.R))
        {
            RestartLevel();
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            ResetData();
        }
        if (AStarGrid != null) AStarGrid.ClearEnemies();

        // ewwww
        var deleteList = new List<Enemy>();
        var deleteList2 = new List<Vector3Int>();
        foreach (var enemy in Enemies)
        {
            if (enemy == null || enemy.Dead || enemy.Health <= 0)
            {
                deleteList.Add(enemy);
            }
            else
            {
                if (AStarGrid != null) AStarGrid.SetEnemy(GroundTileMap.WorldToCell(enemy.transform.position));
            }
        }
        foreach (var d in deleteList)
        {
            Enemies.Remove(d);
        }

        foreach (var h in GroundObjects)
        {
            if (h.Value == null)
            {
                deleteList2.Add(h.Key);
            }
        }
        foreach (var d in deleteList2)
        {
            GroundObjects.Remove(d);
        }
        if (Player != null)
        {
            if (Player.People <= 0)
            {
                EventManager.Trigger("PlayerDead");
                Destroy(Player.gameObject);
                Player = null;
                if (PeopleFixing == 0)
                {
                    StartCoroutine(Lose());
                }
                else
                {
                    EventManager.Trigger("NeedLeave");
                }
            }
            else
            {
                if (GroundTileMap.WorldToCell(Player.transform.position) == GroundTileMap.WorldToCell(Bot.transform.position))
                {
                    Metadata.Scrap += Player.Scrap;
                    Metadata.Food += Player.Food;
                    Metadata.TotalResources += Player.Scrap + Player.Food;
                    Player.Scrap = 0;
                    Player.Food = 0;
                }

                if (CurrentLevel.Final && GroundTileMap.WorldToCell(Player.transform.position) == CurrentLevel.BaseLocation && !finalSequence)
                {
                    Metadata.TotalEndMission = Player.People;
                    Save();
                    StartCoroutine(FinalCountdown());
                }
            }

        }

        if (Bot != null && Bot.Health <= 0)
        {
            if (CurrentLevel.Final)
            {
                Bot.Health = 1;
            }
            else
            {
                Destroy(Bot.gameObject);
                EventManager.Trigger("BotDead");
                if (Metadata.CurrentLevel != LevelProgression.Levels.Length - 1)
                {
                    StartCoroutine(Lose());
                }
            }
        }

        if (activePhase != null && activePhase.completed)
        {
            EndPhase();
        }

        if (Bot != null && !Bot.flying)
        {
            BotShadow.transform.position = Bot.transform.position;
        }
    }

    IEnumerator FinalCountdown()
    {
        Blast.gameObject.SetActive(true);
        Blast.DOFade(0, 0);
        finalSequence = true;
        activePhase.enabled = false;
        yield return new WaitForSeconds(2f);

        Sound.Talk(6);
        EventManager.Trigger("LastSequence1");
        yield return new WaitForSeconds(2f);
        Sound.Play("warning");
        yield return new WaitForSeconds(0.5f);
        Sound.Play("warning");
        yield return new WaitForSeconds(0.5f);
        Sound.Play("warning");
        yield return new WaitForSeconds(0.5f);
        Sound.Talk(3);
        EventManager.Trigger("LastSequence2");
        yield return new WaitForSeconds(2f);
        Sound.Talk(4);
        EventManager.Trigger("LastSequence3");
        yield return new WaitForSeconds(1f);
        Sound.Talk(3);
        EventManager.Trigger("LastSequence4");
        yield return new WaitForSeconds(3f);
        Sound.Talk(6);
        EventManager.Trigger("LastSequence5");
        yield return new WaitForSeconds(2f);

        Sound.Play("start");
        yield return new WaitForSeconds(0.5f);
        Sound.Play("fire2");
        yield return new WaitForSeconds(0.2f);
        Sound.Play("flyin");
        yield return Blast.DOFade(1, 0.1f).WaitForCompletion();
        Blast.DOFade(0, 0.5f);

        Bot.gameObject.SetActive(false);
        foreach (var enemy in Enemies)
        {
            enemy.Health = 0;
        }

        yield return new WaitForSeconds(4f);
        UnityEngine.SceneManagement.SceneManager.LoadScene("Outro");
    }

    IEnumerator Lose()
    {
        yield return new WaitForSeconds(1);
        //fader.DOFade(0.5f, 0.5f);
        GameOverDialog.SetActive(true);
        GameOverDialog.transform.position -= new Vector3(0, 10, 0);
        GameOverDialog.transform.DOMoveY(GameOverDialog.transform.position.y + 10, 2);
        EventManager.Trigger("Lost");
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
    public void ResetData()
    {
        PlayerPrefs.DeleteAll();
        RestartLevel();
    }

    void EndPhase()
    {
        activePhase.completed = false;
        activePhase.enabled = false;
        if (activePhase is PlanningPhase)
        {
            activePhase = BattlePhase;
            activePhase.enabled = true;
        }
        else if (activePhase is BattlePhase)
        {
            StartCoroutine(CompleteLevel());
        }
    }
    public IEnumerator CompleteLevel()
    {
        Metadata.PeopleOnboard = PeopleFixing;
        Metadata.PeopleOnboard += Player.People;
        Metadata.BotHealth = Mathf.Max(10, Bot.Health - 5);
        Metadata.CurrentLevel++;
        Save();

        if (Player != null)
        {
            Player.gameObject.SetActive(false);
            Destroy(Player.gameObject);
        }
        foreach (var enemy in Enemies)
        {
            enemy.GetComponent<SpriteRenderer>().DOFade(0, 0.3f).OnComplete(() => Destroy(enemy.gameObject));
        }
        Enemies.Clear();
        foreach (var gr in GroundObjects)
        {
            gr.Value.GetComponent<SpriteRenderer>().DOFade(0, 0.3f).OnComplete(() => Destroy(gr.Value));
        }
        GroundObjects.Clear();

        if (Bot != null)
        {
            Sound.Play("flyout");
            Bot.Fly(true);
            Bot.transform.DOMoveY(10, 1).SetEase(Ease.InQuad);
            yield return new WaitForSeconds(0.8f);
            yield return Bot.GetComponent<SpriteRenderer>().DOFade(0, 0.2f).WaitForCompletion();
        }
        Destroy(Bot.gameObject);



        if (Metadata.CurrentLevel < LevelProgression.Levels.Length)
        {
            yield return StartLevel();
        }
        else
        {
            SceneManager.LoadScene("Win");
        }

    }

    public IEnumerator StartLevel()
    {
        yield return new WaitForSeconds(0.5f);
        Load();

        TurnsPassed = 0;
        CurrentLevel = LevelProgression.Levels[Metadata.CurrentLevel];
        EventManager.Trigger("lvlStart");
        if (CurrentLevel.Final)
            yield return FillFinalMap();
        else
            yield return FillMap();

        if (!Metadata.SeenTutorial)
        {
            yield return new WaitForSeconds(0.5f);
            Metadata.SeenTutorial = true;
            TutorialContainer.SetActive(true);
            TutorialContainer.GetComponent<Tutorial>().ShowNext();
        }

        activePhase = PlanningPhase;
        activePhase.enabled = true;
    }

    IEnumerator FillFinalMap()
    {
        preventUpdate = true;
        FinalMap.gameObject.SetActive(true);
        usedTiles.Clear();

        FinalMap.CompressBounds();
        BoundsInt bounds = FinalMap.cellBounds;// new BoundsInt(0, 0, 0, 21, 19, 0);
        TileBase[] allTiles = FinalMap.GetTilesBlock(bounds);

        CurrentLevel.Width = bounds.xMax;
        CurrentLevel.Height = bounds.yMax;
        var mapScale = 21f / CurrentLevel.Width;
        MapContainer.transform.localScale = new Vector3(mapScale, mapScale, mapScale);
        var StartingPoint = CurrentLevel.StartingPoint == Vector3Int.zero ? new Vector3Int(CurrentLevel.Width / 2, CurrentLevel.Height / 2, 0) : CurrentLevel.StartingPoint;
        GroundObjects = new Dictionary<Vector3Int, GameObject>();
        GroundData = new GroundTileModel[CurrentLevel.Width, CurrentLevel.Height];

        for (int x = 0; x < bounds.size.x; x++)
        {
            for (int y = 0; y < bounds.size.y; y++)
            {
                TileBase tile = allTiles[x + y * bounds.size.x];

                switch (tile?.name)
                {
                    case "grass1":
                    case "grass2":
                    case "grass3":
                    case "grass4":
                    default:
                        switch (tile?.name)
                        {
                            case "base1":
                                var obj = Instantiate(GroundObjectPrefab);
                                GroundObjects[new Vector3Int(x, y, 0)] = obj;
                                obj.transform.SetParent(MapContainer.transform, false);
                                obj.transform.position = GroundTileMap.CellToWorld(new Vector3Int(x, y, 0));
                                obj.GetComponent<GroundObject>().LoadData(GroundObjectsType.Base);
                                CurrentLevel.BaseLocation = new Vector3Int(x, y, 0);
                                break;
                            case "house1":
                                var obj1 = Instantiate(GroundObjectPrefab);
                                GroundObjects[new Vector3Int(x, y, 0)] = obj1;
                                obj1.transform.SetParent(MapContainer.transform, false);
                                obj1.transform.position = GroundTileMap.CellToWorld(new Vector3Int(x, y, 0));
                                obj1.GetComponent<GroundObject>().LoadData(GroundObjectsType.Residence);
                                break;
                            case "enemy1":
                            case "enemy2":
                            case "enemy4":
                                var obj2 = Instantiate(EnemyPrefab);
                                Enemies.Add(obj2.GetComponent<Enemy>());
                                obj2.transform.SetParent(MapContainer.transform, false);
                                obj2.transform.position = GroundTileMap.CellToWorld(new Vector3Int(x, y, 0));
                                obj2.GetComponent<Enemy>().LoadData(tile.name == "enemy1" ? EnemyType.Basic : tile.name == "enemy2" ? EnemyType.Watcher : EnemyType.Fatty);
                                break;
                        }
                        var tileData = Tiles.GroundTiles[UnityEngine.Random.Range(0, Tiles.GroundTiles.Length)];
                        GroundData[x, y] = tileData;
                        GroundTileMap.SetTile(new Vector3Int(x, y, 0), tileData.tile);
                        break;
                    case "mountains1":
                        var tileData1 = Tiles.MountainTiles[UnityEngine.Random.Range(0, Tiles.MountainTiles.Length)];
                        GroundData[x, y] = tileData1;
                        GroundTileMap.SetTile(new Vector3Int(x, y, 0), tileData1.tile);
                        break;
                }
            }
        }
        AStarGrid = new AStarGrid(CurrentLevel.Width, CurrentLevel.Height, GroundData);

        yield return InstantiateBot(StartingPoint);
        yield return InstantiatePlayer(StartingPoint);
        preventUpdate = false;
        FinalMap.gameObject.SetActive(false);
    }

    IEnumerator FillMap()
    {
        FinalMap.gameObject.SetActive(false);
        GroundTileMap.gameObject.SetActive(true);
        usedTiles.Clear();
        if (CurrentLevel.Width < 10) throw new System.Exception("Too smol level");
        var mapScale = 21f / CurrentLevel.Width;
        MapContainer.transform.localScale = new Vector3(mapScale, mapScale, mapScale);
        var StartingPoint = CurrentLevel.StartingPoint == Vector3Int.zero ? new Vector3Int(CurrentLevel.Width / 2, CurrentLevel.Height / 2, 0) : CurrentLevel.StartingPoint;

        GenerateGround(StartingPoint);
        GenerateObjects(StartingPoint);
        GenerateRoads();
        AStarGrid = new AStarGrid(CurrentLevel.Width, CurrentLevel.Height, GroundData);

        yield return InstantiateBot(StartingPoint);
        yield return InstantiatePlayer(StartingPoint);
        InstantiateEnemies();
    }
    private IEnumerator InstantiateBot(Vector3Int StartingPoint)
    {
        GroundTileMap.gameObject.SetActive(true);
        var botPos = StartingPoint;
        Bot = Instantiate(BotPrefab).GetComponent<Bot>();
        Bot.transform.SetParent(MapContainer.transform, false);
        var targetPos = GroundTileMap.CellToWorld(botPos);
        Bot.LoadData(Metadata);
        Bot.transform.position = targetPos + new Vector3(0, 20, 0);
        BotShadow.transform.position = targetPos;
        Bot.Fly(true);
        Sound.Play("flyin");
        yield return Bot.transform.DOMoveY(targetPos.y, 1).SetEase(Ease.OutQuad).WaitForCompletion();
        Bot.Fly(false);
    }
    private IEnumerator InstantiatePlayer(Vector3Int StartingPoint)
    {
        var playerPos = StartingPoint + new Vector3Int(UnityEngine.Random.value > 0.5f ? 0 : 1, -1, 0);
        usedTiles.Add(playerPos);
        Player = Instantiate(PlayerPrefab).GetComponent<Player>();
        Player.transform.SetParent(MapContainer.transform, false);
        var targetPos = GroundTileMap.CellToWorld(playerPos);
        Player.transform.position = Bot.transform.position;
        yield return new WaitForSeconds(0);
        // sr = Player.GetComponent<SpriteRenderer>();
        //sr.color = new Color(1, 1, 1, 0);
        //Player.transform.DOMove(targetPos, 0.3f);
        //yield return sr.DOFade(1, 0.3f).WaitForCompletion();
    }
    private void InstantiateEnemies()
    {
        Enemies.Clear();
        var possibleTiles = GroundTileMap.GetTilesNearBorder(CurrentLevel.EnemiesDistanceFromBorder);
        foreach (var enType in CurrentLevel.Enemies)
        {
            for (int x = 0; x < enType.Value; x++)
            {
                Vector3Int randomPos = GetFreeRandomPos(possibleTiles);
                var obj = Instantiate(EnemyPrefab);
                Enemies.Add(obj.GetComponent<Enemy>());
                obj.transform.SetParent(MapContainer.transform, false);
                obj.transform.position = GroundTileMap.CellToWorld(randomPos);
                obj.GetComponent<Enemy>().LoadData(enType.Key);
            }
        }
    }

    public List<Vector3Int> GetLowBorder()
    {
        var result = new List<Vector3Int>();
        for (int i = 0; i < CurrentLevel.Width; i++)
        {
            result.Add(new Vector3Int(i, 0, 0));
        }
        return result;
    }
    public List<Vector3Int> GetSpawnPositions()
    {
        if (Enemies.Count >= 15) return new List<Vector3Int>();

        var amount = Mathf.Min(15 - Enemies.Count, Helpers.Pick(CurrentLevel.SDSpawnSpread) + TurnsPassed / 20);
        var possibleTiles = CurrentLevel.Final ? GetLowBorder() : GroundTileMap.GetTilesNearBorder(1);
        var list = new List<Vector3Int>();
        foreach (var v in possibleTiles)
        {
            if (!usedTiles.Contains(v)) list.Add(v);
        }
        return Helpers.PickRange(list, amount, true);
    }
    public void AddEnemies(List<Vector3Int> list)
    {
        foreach (var pos in list)
        {
            // privet, menya zovut e-type, na zdorovieeeee
            var eType = Helpers.Pick(CurrentLevel.Enemies);
            var obj = Instantiate(EnemyPrefab);
            obj.transform.SetParent(MapContainer.transform, false);
            var targetPos = GroundTileMap.CellToWorld(pos);
            obj.transform.position = targetPos + (Bot.transform.position - pos).normalized * -2;
            obj.transform.DOMove(targetPos, 0.3f);
            obj.GetComponent<Enemy>().LoadData(eType.Key);
            Enemies.Add(obj.GetComponent<Enemy>());
        }
    }
    public void GenerateGround(Vector3Int startingPoint)
    {
        GroundData = new GroundTileModel[CurrentLevel.Width, CurrentLevel.Height];
        GroundTileMap.ClearAllTiles();

        for (int x = 0; x < CurrentLevel.Width; x++)
        {
            for (int y = 0; y < CurrentLevel.Height; y++)
            {
                var tileData = Tiles.GroundTiles[UnityEngine.Random.Range(0, Tiles.GroundTiles.Length)];
                GroundData[x, y] = tileData;
                GroundTileMap.SetTile(new Vector3Int(x, y, 0), tileData.tile);
            }
        }
        var possibleTiles = GroundTileMap.GetTilesExceptNearPoint(startingPoint, true, 3);
        for (int x = 0; x < CurrentLevel.Mountains; x++)
        {
            Vector3Int randomPos = GetFreeRandomPos(possibleTiles);
            var tileData = Tiles.MountainTiles[UnityEngine.Random.Range(0, Tiles.MountainTiles.Length)];
            GroundData[randomPos.x, randomPos.y] = tileData;
            GroundTileMap.SetTile(randomPos, tileData.tile);
            usedTiles.Add(randomPos);
        }
    }
    private void GenerateRoads()
    {

    }
    public void GenerateObjects(Vector3Int startingPoint)
    {
        GroundObjects = new Dictionary<Vector3Int, GameObject>();
        var possibleTiles = GroundTileMap.GetTilesExceptNearPoint(startingPoint, false, 2);
        foreach (var objData in CurrentLevel.Objects)
        {
            for (int x = 0; x < objData.Value; x++)
            {
                Vector3Int randomPos = GetFreeRandomPos(possibleTiles);
                var obj = Instantiate(GroundObjectPrefab);
                GroundObjects[randomPos] = obj;
                obj.transform.SetParent(MapContainer.transform, false);
                obj.transform.position = GroundTileMap.CellToWorld(randomPos);
                obj.GetComponent<GroundObject>().LoadData(objData.Key);
            }
        }

        for (int x = 0; x < CurrentLevel.Survivors; x++)
        {
            Helpers.Pick(GroundObjects.Values).GetComponent<GroundObject>().AddSurvivors(1);
        }

        for (int x = 0; x < CurrentLevel.Survivors; x++)
        {
            Helpers.Pick(GroundObjects.Values).GetComponent<GroundObject>().AddSurvivors(1);
        }


    }

    private Vector3Int getRandomPos(List<Vector3Int> list)
    {
        return list[UnityEngine.Random.Range(0, list.Count)];
    }

    private Vector3Int GetFreeRandomPos(List<Vector3Int> possibleTiles)
    {
        Vector3Int randomPos = getRandomPos(possibleTiles);
        while (usedTiles.Contains(randomPos)) { randomPos = getRandomPos(possibleTiles); }

        return randomPos;
    }

    private Vector3Int getRandomPos(int width, int height)
    {
        return new Vector3Int(UnityEngine.Random.Range(0, width), UnityEngine.Random.Range(0, height), 0);
    }
}
