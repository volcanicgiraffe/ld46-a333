﻿internal interface IPhase
{
    bool enabled { get; set; }
    bool completed { get; set; }
}