﻿using System;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class GroundObject : MonoBehaviour
{
    public Sprite[] HouseSprites = new Sprite[0];
    public Sprite MallSprite;
    public Sprite FactorySprite;
    public Sprite BaseSprite;
    public GroundObjectsType type;

    public int Scrap;
    public int Food;
    public int Survivors;

    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            LevelBrain.Instance.Sound.Play("pickup");
            var pl = other.GetComponent<Player>();
            pl.Scrap += Scrap;
            pl.Food += Food;
            pl.People += Survivors;
            EventManager.Trigger("PickedUpStuff", new int[] { Scrap, Food });
            if (Survivors > 0)
            {
                EventManager.Trigger("PickedUpSurvivors", Survivors);
                LevelBrain.Instance.Metadata.TotalSurvivorsSaved += Survivors;
            }
            Scrap = 0;
            Food = 0;
            Survivors = 0;
            GetComponent<Collider2D>().enabled = false;
            transform.GetChild(0).gameObject.SetActive(false);
            transform.GetChild(1).gameObject.SetActive(true);
        }
    }

    public void LoadData(GroundObjectsType type)
    {
        var sr = GetComponent<SpriteRenderer>();
        sr.sortingOrder = (int)Mathf.Round(-transform.position.y);
        this.type = type;
        switch (type)
        {
            case GroundObjectsType.Residence:
                sr.sprite = HouseSprites[UnityEngine.Random.Range(0, HouseSprites.Length)];
                Scrap = Helpers.Pick(new int[] { 0, 1, 1, 1, 2, 2, 2 });
                Food = Helpers.Pick(new int[] { 0, 0, 0, 1, 1, 1, 1 });
                break;
            case GroundObjectsType.Mall:
                sr.sprite = MallSprite;
                Scrap = Helpers.Pick(new int[] { 0, 0, 0, 0, 1, 1, 1 });
                Food = UnityEngine.Random.Range(5, 10);
                break;
            case GroundObjectsType.Factory:
                sr.sprite = FactorySprite;
                Scrap = UnityEngine.Random.Range(10, 15);
                Food = Helpers.Pick(new int[] { 0, 0, 0, 0, 1, 1, 1 });
                break;
            case GroundObjectsType.Base:
                sr.sprite = BaseSprite;
                Scrap = UnityEngine.Random.Range(30, 35);
                Food = 50;
                break;
        }

    }

    internal void AddSurvivors(int v)
    {
        Survivors += v;
        transform.GetChild(0).gameObject.SetActive(true);
    }
}
