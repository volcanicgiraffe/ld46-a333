﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class WonBrain : MonoBehaviour
{
    public GameObject start;
    public SpriteRenderer Star;
    public SpriteRenderer fader;
    public SpriteRenderer won;
    public SpriteRenderer under;
    public AudioClip sndWhoosh;
    public AudioClip sndStart;
    AudioSource aus;

    public TMPro.TextMeshProUGUI Stats;

    // Start is called before the first frame update
    void Start()
    {
        aus = GetComponent<AudioSource>();
        StartCoroutine(Animate());
    }

    private IEnumerator Animate()
    {
        var stPos = Star.transform.position;
        Star.transform.position += new Vector3(0, 15);
        Star.DOFade(1, 1f);
        yield return Star.transform.DOMove(stPos, 1f).SetEase(Ease.Linear).WaitForCompletion();

        won.DOFade(1, 0.1f);
        aus.PlayOneShot(sndWhoosh);
        ShowStats();
    }

    private void ShowStats()
    {
        var data = PlayerPrefs.GetString("data");
        GameMetadata md;
        if (!String.IsNullOrEmpty(data))
        {
            md = JsonUtility.FromJson<GameMetadata>(data);
        }
        else
        {
            md = new GameMetadata();
        }

        under.DOFade(1, 0.1f);
        Stats.text = $@"
Total turns: {md.TotalTurns}                  
Resources found: {md.TotalResources}
Enemies killed: {md.TotalKilled}
Survivors Saved: {md.TotalSurvivorsSaved}
People died: {md.TotalPeopleDied}
People made it to Condor: {md.TotalEndMission}
                                                                   TOTAL SCORE: {md.TotalEndMission * 100 + md.TotalKilled * 5 + md.TotalResources + md.TotalSurvivorsSaved * 10 - md.TotalTurns - md.TotalPeopleDied * 10}
";
    }

    public void StartGame()
    {
        PlayerPrefs.DeleteAll();
        StartCoroutine(startGameC());
    }

    IEnumerator startGameC()
    {
        aus.PlayOneShot(sndStart);
        fader.DOFade(1, 0.5f);
        yield return new WaitForSeconds(1);
        UnityEngine.SceneManagement.SceneManager.LoadScene("Level");
    }

    // Update is called once per frame
    void Update()
    {

    }
}
