﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
public class RangeHighlighter : MonoBehaviour
{
    public GameObject RangeCursorPrefab;
    public Dictionary<Vector3Int, GameObject> Ranges = new Dictionary<Vector3Int, GameObject>();
    public void ShowRadius(Vector3Int center, int width, int height, int range, bool diagonals, Color color)
    {
        Hide();
        for (int x = -range; x < range + 1; x++)
        {
            for (int y = -range; y < range + 1; y++)
            {
                if (x == 0 && y == 0) continue;
                var tilepos = center + new Vector3Int(x, y, 0);
                if (tilepos.x < 0 || tilepos.y < 0 || tilepos.x >= width || tilepos.y >= height) continue;
                var dist = diagonals ? tilepos.ChebyshevDistance(center) : tilepos.ManhattanDistance(center);
                if (dist <= range)
                {
                    AddSprite(color, tilepos);
                }
            }
        }
    }

    private void AddSprite(Color color, Vector3Int tilepos)
    {
        if (Ranges.ContainsKey(tilepos)) return;
        var hl = Instantiate(RangeCursorPrefab);
        foreach (var sr in hl.GetComponentsInChildren<SpriteRenderer>())
        {
            sr.color = color;
        }
        hl.transform.SetParent(transform, false);
        hl.transform.localPosition = new Vector3(tilepos.x, tilepos.y);
        Ranges.Add(tilepos, hl);
    }

    public void ShowAll(ICollection<Vector3Int> tiles, int width, int height, Color color)
    {
        Hide();
        foreach (var pos in tiles)
        {
            if (pos.x < 0 || pos.y < 0 || pos.x >= width || pos.y >= height) continue;
            AddSprite(color, pos);
        }
    }

    public bool InRange(Vector3Int pos)
    {
        return Ranges.ContainsKey(new Vector3Int(pos.x, pos.y, 0));
    }

    //private Vector3Int getLocalV3Int()
    //{
    //    return new Vector3Int((int)Mathf.Round(transform.position.x), (int)Mathf.Round(transform.position.y), 0);
    //}

    public void Hide()
    {
        foreach (GameObject go in Ranges.Values)
        {
            Destroy(go);
        }
        Ranges.Clear();
    }


}


